# Shopping Cart #

You have been contracted to build the shopping cart system.

We will start with the following tours in our database

| Id   |     Name              |  Price  |
| ---- | --------------------  | -------:|
| OH   | "Opera house tour"    | $300.00 |
| BC   | "Sydney Bridge Climb" | $110.00 |
| SK   | "Sydney Sky Tower"    |  $30.00 |

As we want to attract attention, we intend to have a few weekly specials. • We are going to have a 3 for 2 deal on opera house ticket. For example, if you buy 3 tickets, you will pay the price of 2 only getting another one completely free of charge. • We are going to give a free Sky Tower tour for with every Opera House tour sold • The Sydney Bridge Climb will have a bulk discount applied, where the price will drop to $20, if someone buys more than 4

As our Online Sales team is quite indecisive, we want the promotional rules to be as flexible as possible as they will change in the future.

Our Shopping Cart system can have items added in any order.

Your task is to implement a shopping cart system that meets the requirements described above.

For example Items added to the cart: OH, OH, OH, BC - Total expected: $710.00 Items added to the cart: OH, SK - Total expected: $300.00 Items added to the cart: BC, BC, BC, BC, BC, OH - Total expected: $750

Instructions •No user interface or command line is required – we are interested in how you solve the problem only. • Please do not use any frameworks unless it is for testing. • Please note any assumptions

### Assumptions: ###

* Commits are not squashed in order to show creation process.
* All prices are in AUD.
* Anti-Corruption Layer was omitted. Normally this would prevent to leak implementation details and expose immutable transport objects. I could then easy change domain entities with minimal risk to break external components (like adapters in port and adapters architecture e.g REST API, SOAP, GUI, command line or implementation of repository or messaging systems)
* Tour Id is unique in repository. Adding new tour types by administrator user is not validated at this time. 
* In order to decouple dependencies between shopping cart and promotion calculation construction shopping cart does not contain promotion rules.
* ITourBookingService is API delivered to interact with created tour booking system from external components. Endpoint getBookedToursSummary expose ShoppingCart including promotions.
* Discount policy for bulk promotion discount value should not be bigger than discounted tour price.
* Created discount policies are initialized in PromotionalRuleFactory for integration test purposes. Normally should be initialized in application lifetime. PromotionalRules creates flexible chain of responsibility in order to apply discount rules. Solution like this allows to create chained discount rules depending on policies in runtime.