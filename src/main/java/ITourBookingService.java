import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.cart.ShoppingCartNotFoundException;
import com.borowik.shop.tour.Tour;

import java.util.Collection;

/**
 * API for external components for creating shopping cart and booking tours
 */
public interface ITourBookingService {
    /**
     * @return all tours which are available in current timeline
     */
    Collection<Tour> getAvailableTours();

    /**
     *
     * @param tourId alphabetic identity for specify tour
     * @return Tour identified though @param tourId
     */
    Tour getTour(String tourId);

    /**
     * Create new cart and return id of cart
     * @return identity of already created cart
     */
    String createCart();

    /**
     * Adds tour to the cart. Unknown tourId will not be added.
     * @param tourId alphabetic identity for specify tour
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId with recently added tour to this cart.
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not exist
     */
    ShoppingCart bookTour(String tourId, String shoppingCartId) throws ShoppingCartNotFoundException;

    /**
     *
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not exist
     */
    ShoppingCart getBookedToursSummary(String shoppingCartId) throws ShoppingCartNotFoundException;

    /**
     *
     * @param tourId alphabetic identity for specify tour
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId with recently removed tour to this cart
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not exist
     */
    ShoppingCart removeTourFromCart(String tourId, String shoppingCartId) throws ShoppingCartNotFoundException;

    /**
     *
     * @param shoppingCartId shoppingCartId numeric identity for specify shopping cart
     * @return true if cart is cleared, false if cart not cleared
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not exist
     */
    boolean removeAllToursFromCart(String shoppingCartId) throws ShoppingCartNotFoundException;
}
