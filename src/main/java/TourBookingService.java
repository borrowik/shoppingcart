import com.borowik.shop.cart.IShoppingCartRepository;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.cart.ShoppingCartNotFoundException;
import com.borowik.shop.promotion.PromotionService;
import com.borowik.shop.tour.ITourRepository;
import com.borowik.shop.tour.Tour;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

public class TourBookingService implements ITourBookingService {

    private static final Logger log = Logger.getLogger(TourBookingService.class.getName());

    private final ITourRepository tourRepository;
    private final IShoppingCartRepository shoppingCartRepository;
    private final PromotionService promotionService;

    public TourBookingService(ITourRepository tourRepository,
                              IShoppingCartRepository shoppingCartRepository,
                              PromotionService promotionService) {
        this.tourRepository = tourRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.promotionService = promotionService;
    }

    @Override
    public Collection<Tour> getAvailableTours() {
        return tourRepository.getAvailableTours();
    }

    @Override
    public Tour getTour(String tourId) {
        return tourRepository.getTour(tourId).orElse(null);
    }

    @Override
    public String createCart() {
        ShoppingCart shoppingCart = shoppingCartRepository.saveShoppingCart(new ShoppingCart());
        return shoppingCart.getShoppingCartId();
    }

    @Override
    public ShoppingCart bookTour(String tourId, String shoppingCartId) throws ShoppingCartNotFoundException {
        Objects.requireNonNull(tourId, "TourId is required");
        final ShoppingCart shoppingCart = shoppingCartRepository.getShoppingCart(shoppingCartId).orElseThrow(() ->
                new ShoppingCartNotFoundException(String.format("Shopping Cart %s not found", shoppingCartId)));
        Optional<Tour> tour = tourRepository.getTour(tourId);
        if(tour.isEmpty()){
            log.info("TourId is unknown. Tour will not be added to cart.");
            return shoppingCart;
        }
        shoppingCart.add(tour.get());
        shoppingCartRepository.saveShoppingCart(shoppingCart);
        return promotionService.calculatePromotions(shoppingCart);
    }

    @Override
    public ShoppingCart getBookedToursSummary(String shoppingCartId) throws ShoppingCartNotFoundException {
        final ShoppingCart shoppingCart = shoppingCartRepository.getShoppingCart(shoppingCartId).orElseThrow(() ->
                new ShoppingCartNotFoundException(String.format("Shopping Cart %s not found", shoppingCartId)));
        return promotionService.calculatePromotions(shoppingCart);
    }

    @Override
    public ShoppingCart removeTourFromCart(String tourId, String shoppingCartId) throws ShoppingCartNotFoundException {
        Objects.requireNonNull(tourId, "TourId is required");
        final ShoppingCart shoppingCart = shoppingCartRepository.getShoppingCart(shoppingCartId).orElseThrow(() ->
                new ShoppingCartNotFoundException(String.format("Shopping Cart %s not found", shoppingCartId)));
        Optional<Tour> tour = shoppingCart.getAllAddedTours().stream().filter(t -> tourId.equals(t.getTourId())).findFirst();
        tour.ifPresent(shoppingCart::remove);
        shoppingCartRepository.saveShoppingCart(shoppingCart);
        return promotionService.calculatePromotions(shoppingCart);
    }

    @Override
    public boolean removeAllToursFromCart(String shoppingCartId) throws ShoppingCartNotFoundException {
        final ShoppingCart shoppingCart = shoppingCartRepository.getShoppingCart(shoppingCartId).orElseThrow(() ->
                new ShoppingCartNotFoundException(String.format("Shopping Cart %s not found", shoppingCartId)));
        shoppingCart.getAllAddedTours().clear();
        return shoppingCartRepository.saveShoppingCart(shoppingCart).getAllAddedTours().isEmpty();
    }
}
