package com.borowik.shop;

import java.time.LocalDate;
import java.util.UUID;

public class IdentityGenerator {

    public static String generateIdentityBasedOnDate() {
        return LocalDate.now().toString() + "-" + UUID.randomUUID().toString();
    }

}