package com.borowik.shop;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

public class Money {

    private final BigDecimal amount;
    private final Currency currency;

    public Money(BigDecimal amount) {
        this.currency = Currency.getInstance("AUD");
        this.amount = Objects.requireNonNull(amount);
    }

    public static Money createMoneyWithAmountZero(){
        return new Money(BigDecimal.valueOf(0));
    }

    public BigDecimal getAmount() {
        return amount;
    }

    Currency getCurrency() {
        return currency;
    }

    public Money add(Money other) {
        return new Money(amount.add(other.amount));
    }

    public Money subtract(Money other) {
        return new Money(amount.subtract(other.amount));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return amount.equals(money.amount) &&
                Objects.equals(currency, money.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }
}
