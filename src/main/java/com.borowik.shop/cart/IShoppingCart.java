package com.borowik.shop.cart;

import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

public interface IShoppingCart {

    void add(Tour tour);

    boolean remove(Tour tour);

    Money total();

}
