package com.borowik.shop.cart;

import java.util.Optional;

public interface IShoppingCartRepository {

    Optional<ShoppingCart> getShoppingCart(String shoppingCartId);

    ShoppingCart saveShoppingCart(ShoppingCart shoppingCart);

}
