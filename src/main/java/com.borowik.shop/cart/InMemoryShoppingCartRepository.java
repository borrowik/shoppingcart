package com.borowik.shop.cart;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

public class InMemoryShoppingCartRepository implements IShoppingCartRepository {

    private static final Logger log = Logger.getLogger(InMemoryShoppingCartRepository.class.getName());

    private final Map<String, ShoppingCart> idToShoppingCartMap = new HashMap<>();

    @Override
    public Optional<ShoppingCart> getShoppingCart(String shoppingCartId) {
        return Optional.ofNullable(idToShoppingCartMap.get(shoppingCartId));
    }

    @Override
    public ShoppingCart saveShoppingCart(ShoppingCart shoppingCart) {
        if(idToShoppingCartMap.containsKey(shoppingCart.getShoppingCartId())) {
            log.warning("ShoppingCart already exists");
        }
        idToShoppingCartMap.put(shoppingCart.getShoppingCartId(), shoppingCart);
        return shoppingCart;
    }
}
