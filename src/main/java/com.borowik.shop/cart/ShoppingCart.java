package com.borowik.shop.cart;

import com.borowik.shop.IdentityGenerator;
import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public class ShoppingCart implements IShoppingCart {

    private final String shoppingCartId;
    private final Collection<Tour> tours;
    private Money totalPrice;

    public ShoppingCart(){
        this.shoppingCartId = IdentityGenerator.generateIdentityBasedOnDate();
        this.tours = new ArrayList<>();
        this.totalPrice = Money.createMoneyWithAmountZero();
    }

    public static ShoppingCart createShoppingCart(String shoppingCartId, Collection<Tour> tours, Money totalPriceAfterPromotion) {
        return new ShoppingCart(shoppingCartId, tours, totalPriceAfterPromotion);
    }

    private ShoppingCart(String shoppingCartId, Collection<Tour> tours, Money money){
        this.shoppingCartId = shoppingCartId;
        this.totalPrice = new Money(money.getAmount());
        this.tours = new ArrayList<>(tours);
    }

    public String getShoppingCartId() {
        return shoppingCartId;
    }

    public Collection<Tour> getAllAddedTours(){
        return tours;
    }

    @Override
    public void add(Tour tour) {
        Objects.requireNonNull(tour);
        tours.add(tour);
        totalPrice = evaluateTotal();
    }

    @Override
    public boolean remove(Tour removeTour) {
        Objects.requireNonNull(removeTour);
        Optional<Tour> tourOptional = getAllAddedTours().stream()
                .filter(t -> removeTour.getTourId().equals(t.getTourId()))
                .findFirst();
        if (tourOptional.isPresent()){
            getAllAddedTours().remove(tourOptional.get());
            totalPrice = evaluateTotal();
            return true;
        }
        return false;
    }

    @Override
    public Money total() {
        return totalPrice;
    }

    private Money evaluateTotal() {
        return new Money(tours.stream()
                .map(Tour::getPrice)
                .map(Money::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }
}
