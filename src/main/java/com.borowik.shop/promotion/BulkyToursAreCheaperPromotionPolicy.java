package com.borowik.shop.promotion;

import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

import java.math.BigDecimal;
import java.util.Objects;

import static com.borowik.shop.cart.ShoppingCart.createShoppingCart;

/**
 * Buy more then 4 tickets for @param discountedTour and get @param discount
 */
@Policy
public class BulkyToursAreCheaperPromotionPolicy  extends PromotionalRules {

    private final Tour discountedTour;
    private final int buyMoreThen;
    private final Money discount;

    BulkyToursAreCheaperPromotionPolicy(Tour discountedTour, int buyMoreThen, Money discount) {
        this.discountedTour = Objects.requireNonNull(discountedTour);
        this.discount = Objects.requireNonNull(discount);
        assert buyMoreThen > 0;
        this.buyMoreThen = buyMoreThen;
    }

    @Override
    public ShoppingCart calculatePromotion(ShoppingCart shoppingCart) {
        long numberOfDiscountTours = shoppingCart.getAllAddedTours().stream()
                .filter(tour -> tour.getTourId().equals(discountedTour.getTourId()))
                .count();
        ShoppingCart cart = shoppingCart;
        if (numberOfDiscountTours > buyMoreThen) {
            Money priceAfterPromotion = shoppingCart.total().subtract(
                    new Money(discount.getAmount().multiply(BigDecimal.valueOf(numberOfDiscountTours))));
            cart = createShoppingCart(shoppingCart.getShoppingCartId(), shoppingCart.getAllAddedTours(), priceAfterPromotion);
        }
        return calculateNext(cart);
    }
}
