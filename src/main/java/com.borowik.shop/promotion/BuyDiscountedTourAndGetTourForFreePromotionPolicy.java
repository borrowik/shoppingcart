package com.borowik.shop.promotion;

import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.borowik.shop.cart.ShoppingCart.createShoppingCart;

/**
 * Buy @param boughtTour and get @param tourForFree for free
 */
@Policy
public class BuyDiscountedTourAndGetTourForFreePromotionPolicy extends PromotionalRules {

    private Tour boughtTour;
    private Tour tourForFree;

    BuyDiscountedTourAndGetTourForFreePromotionPolicy(Tour boughtTour, Tour tourForFree) {
        this.boughtTour = Objects.requireNonNull(boughtTour);
        this.tourForFree = Objects.requireNonNull(tourForFree);
    }

    @Override
    public ShoppingCart calculatePromotion(ShoppingCart shoppingCart) {
        if(shoppingCart.getAllAddedTours().isEmpty() || !isDiscountedTourInCart(shoppingCart)){
            return shoppingCart;
        }
        List<Tour> tours = shoppingCart.getAllAddedTours().stream()
                .filter(tour -> tour.getTourId().equals(boughtTour.getTourId()))
                .map(tour -> tourForFree)
                .collect(Collectors.toList());
        Money totalPriceBeforePromotion = shoppingCart.total();
        tours.addAll(shoppingCart.getAllAddedTours());
        return calculateNext(createShoppingCart(shoppingCart.getShoppingCartId(), tours, totalPriceBeforePromotion));
    }

    private boolean isDiscountedTourInCart(ShoppingCart shoppingCart) {
        return shoppingCart.getAllAddedTours().stream().anyMatch(tour -> boughtTour.getTourId().equals(tour.getTourId()));
    }
}
