package com.borowik.shop.promotion;

import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

import java.util.Collection;
import java.util.Objects;

import static com.borowik.shop.cart.ShoppingCart.createShoppingCart;

/**
 * Discount tour for free when tour multiple numberOfTours in cart.
 * @param discountedTour tour that is discounted.
 * @param numberOfTours is minimal number of tours to apply discount.
 */
@Policy
public class FreeTourForMultipleTourBookingPromotionPolicy extends PromotionalRules {

    private final Tour discountedTour;
    private final int numberOfTours;

    FreeTourForMultipleTourBookingPromotionPolicy(Tour discountedTour, int numberOfTours) {
        Objects.requireNonNull(discountedTour);
        assert numberOfTours > 0;
        this.discountedTour = discountedTour;
        this.numberOfTours = numberOfTours;
    }

    @Override
    public ShoppingCart calculatePromotion(ShoppingCart shoppingCart) {
        Collection<Tour> toursInCart = shoppingCart.getAllAddedTours();
        if (toursInCart.size() < numberOfTours) {
            return shoppingCart;
        }

        long numberOfSpecifiedTours = toursInCart.stream()
                .filter(tour -> tour.getTourId().equals(discountedTour.getTourId()))
                .count();
        int numberOfFreeTours = (int) numberOfSpecifiedTours / numberOfTours;
        Money discount = Money.createMoneyWithAmountZero();
        for (int i = 0; i < numberOfFreeTours; i++) {
            discount = discount.add(discountedTour.getPrice());
        }
        Money totalPriceAfterPromotion = shoppingCart.total().subtract(discount);
        return calculateNext(
                createShoppingCart(
                        shoppingCart.getShoppingCartId()
                        , shoppingCart.getAllAddedTours()
                        , totalPriceAfterPromotion));

    }

}
