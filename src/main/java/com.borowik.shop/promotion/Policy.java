package com.borowik.shop.promotion;

/**
 * Annotation used for policies which describe changeable business rules
 */
@interface Policy {
}
