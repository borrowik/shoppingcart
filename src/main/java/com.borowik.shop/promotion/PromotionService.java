package com.borowik.shop.promotion;

import com.borowik.shop.cart.ShoppingCart;

public class PromotionService {

    private final PromotionalRuleFactory ruleFactory;

    public PromotionService(PromotionalRuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    public ShoppingCart calculatePromotions(ShoppingCart shoppingCart) {
        return calculatePromotions(shoppingCart, ruleFactory.createPromotionalRules());
    }

    private ShoppingCart calculatePromotions(ShoppingCart shoppingCart, PromotionalRules promotions) {
        return promotions != null ? promotions.calculatePromotion(shoppingCart) : shoppingCart;
    }

}
