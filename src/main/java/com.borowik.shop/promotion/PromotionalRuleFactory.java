package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;

import java.math.BigDecimal;

public class PromotionalRuleFactory {

    PromotionalRules createPromotionalRules() {
        Tour tourOne = new Tour("OH", "Opera house tour", new Money(BigDecimal.valueOf(300)));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.valueOf(110)));
        Tour tourThree = new Tour("SK", "Sydney Sky Tower", new Money(BigDecimal.valueOf(30)));
        PromotionalRules rules = new BulkyToursAreCheaperPromotionPolicy(tourThree, 4,new Money(BigDecimal.valueOf(10)));
        rules.addNextPromotionRule(new BuyDiscountedTourAndGetTourForFreePromotionPolicy(tourOne, tourTwo))
                        .addNextPromotionRule(new FreeTourForMultipleTourBookingPromotionPolicy(tourOne,3));
        return rules;
    }
}
