package com.borowik.shop.promotion;


import com.borowik.shop.cart.ShoppingCart;

public abstract class PromotionalRules {

    private PromotionalRules nextPromotionalRules;

    PromotionalRules addNextPromotionRule(PromotionalRules nextPromotionalRules){
        this.nextPromotionalRules = nextPromotionalRules;
        return nextPromotionalRules;
    }

    public abstract ShoppingCart calculatePromotion(ShoppingCart shoppingCart);

    ShoppingCart calculateNext(ShoppingCart shoppingCart){
        if (nextPromotionalRules == null) {
            return shoppingCart;
        }
        return nextPromotionalRules.calculatePromotion(shoppingCart);
    }

    PromotionalRules getNextPromotionalRules() {
        return nextPromotionalRules;
    }

}
