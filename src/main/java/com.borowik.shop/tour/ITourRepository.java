package com.borowik.shop.tour;

import java.util.Collection;
import java.util.Optional;

public interface ITourRepository {

    void add(Tour tour);

    boolean remove(Tour tour);

    Collection<Tour> getAvailableTours();

    Optional<Tour> getTour(String tourId);

}
