package com.borowik.shop.tour;

import java.util.*;
import java.util.logging.Logger;

public class InMemoryTourRepository implements ITourRepository {

    private static final Logger log = Logger.getLogger(InMemoryTourRepository.class.getName());

    private final Map<String, Tour> idToTourMap = new HashMap<>();

    @Override
    public void add(Tour tour) {
        if(idToTourMap.containsKey(tour.getTourId())) {
            log.warning("Tour already exists");
            return;
        }
        idToTourMap.put(tour.getTourId(), tour);
    }

    @Override
    public boolean remove(Tour tour) {
        if(idToTourMap.containsKey(tour.getTourId())){
            idToTourMap.remove(tour.getTourId());
            return true;
        }
        log.warning("Tour not exists");
        return false;
    }

    @Override
    public Collection<Tour> getAvailableTours() {
        return idToTourMap.values();
    }

    @Override
    public Optional<Tour> getTour(String tourId) {
        return Optional.ofNullable(idToTourMap.get(tourId));
    }
}
