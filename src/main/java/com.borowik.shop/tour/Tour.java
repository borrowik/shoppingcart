package com.borowik.shop.tour;

import com.borowik.shop.Money;

import java.util.Objects;

public class Tour {

    private final String tourId;
    private final String tourName;
    private final Money price;

    public Tour(String tourId, String tourName, Money price) {
        this.tourId = Objects.requireNonNull(tourId);
        this.tourName = Objects.requireNonNull(tourName);
        this.price = Objects.requireNonNull(price);
    }

    public String getTourId() {
        return tourId;
    }

    public String getTourName() {
        return tourName;
    }

    public Money getPrice() {
        return price;
    }

}
