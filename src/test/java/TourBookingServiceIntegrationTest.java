import com.borowik.shop.Money;
import com.borowik.shop.cart.IShoppingCartRepository;
import com.borowik.shop.cart.InMemoryShoppingCartRepository;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.promotion.PromotionService;
import com.borowik.shop.promotion.PromotionalRuleFactory;
import com.borowik.shop.tour.ITourRepository;
import com.borowik.shop.tour.InMemoryTourRepository;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

class TourBookingServiceIntegrationTest {

    private ITourRepository tourRepository = new InMemoryTourRepository();
    private IShoppingCartRepository shoppingCartRepository = new InMemoryShoppingCartRepository();
    private PromotionService promotionService = new PromotionService(new PromotionalRuleFactory());
    private ITourBookingService service;

    @BeforeEach
    void setUp() {
        service = new TourBookingService(tourRepository, shoppingCartRepository, promotionService);
    }

    @Test
    void createSingleCart() {
        // when
        String shoppingCartId = service.createCart();

        // then
        assertThat(shoppingCartRepository.getShoppingCart(shoppingCartId).isPresent(), is(true));
        assertThat(shoppingCartRepository.getShoppingCart(shoppingCartId).get().getShoppingCartId(), is(shoppingCartId));
    }

    @Test
    void createAvailableTours() {
        // given
        Tour tour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));

        // when
        tourRepository.add(tour);
        Tour existingTour = service.getTour(tour.getTourId());
        Collection<Tour> availableTours = service.getAvailableTours();

        // then
        assertThat(existingTour, is(tour));
        assertThat(availableTours.size(), is(1));
        assertThat(availableTours, hasItem(tour));
    }

    @Test
    void bookSingleTour() {
        // given
        Tour tour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        tourRepository.add(tour);
        String shoppingCartId = service.createCart();

        // when
        ShoppingCart shoppingCart = service.bookTour(tour.getTourId(), shoppingCartId);

        // then
        assertThat(shoppingCart.getShoppingCartId(), is(shoppingCartId));
        assertThat(shoppingCart.getAllAddedTours(), hasItem(tour));
        assertThat(
                shoppingCartRepository.getShoppingCart(shoppingCartId)
                        .map(ShoppingCart::getShoppingCartId)
                        .orElse(null)
                , is(shoppingCartId));
        assertThat(shoppingCart.total(), is(notNullValue()));
        assertThat(shoppingCart.total().getAmount(), is(BigDecimal.ONE));
    }

    @Test
    void createBookedToursSummaryForTwoTours() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.TEN));
        tourRepository.add(tourOne);
        tourRepository.add(tourTwo);
        String shoppingCartId = service.createCart();

        // when
        service.bookTour(tourOne.getTourId(), shoppingCartId);
        service.bookTour(tourTwo.getTourId(), shoppingCartId);
        ShoppingCart shoppingCart = service.getBookedToursSummary(shoppingCartId);

        // then
        assertThat(shoppingCart.getShoppingCartId(), is(shoppingCartId));
        assertThat(shoppingCart.getAllAddedTours().size(), is(2));
        assertThat(shoppingCart.getAllAddedTours(), hasItems(tourOne, tourTwo));
        assertThat(shoppingCart.total(), is(notNullValue()));
        assertThat(shoppingCart.total().getAmount(), is(BigDecimal.valueOf(11)));
    }

    @Test
    void createBookedToursSummaryWithDiscount() {
        // given
        Tour tourOne = new Tour("OH", "Opera house tour", new Money(BigDecimal.valueOf(300)));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.valueOf(110)));
        Tour tourThree = new Tour("SK", "Sydney Sky Tower", new Money(BigDecimal.valueOf(30)));
        tourRepository.add(tourOne);
        tourRepository.add(tourTwo);
        tourRepository.add(tourThree);
        String shoppingCartId = service.createCart();

        // when
        service.bookTour(tourOne.getTourId(), shoppingCartId);
        service.bookTour(tourOne.getTourId(), shoppingCartId);
        service.bookTour(tourOne.getTourId(), shoppingCartId);
        service.bookTour(tourTwo.getTourId(), shoppingCartId);
        ShoppingCart shoppingCart = service.getBookedToursSummary(shoppingCartId);

        // then
        assertThat(shoppingCart.getShoppingCartId(), is(shoppingCartId));
        assertThat(shoppingCart.getAllAddedTours().size(), is(7));
        assertThat(shoppingCart.total().getAmount(), is(BigDecimal.valueOf(710)));
    }

}