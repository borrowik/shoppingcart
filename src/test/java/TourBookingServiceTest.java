import com.borowik.shop.Money;
import com.borowik.shop.cart.IShoppingCartRepository;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.cart.ShoppingCartNotFoundException;
import com.borowik.shop.promotion.PromotionService;
import com.borowik.shop.tour.ITourRepository;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class TourBookingServiceTest {

    @Mock
    private ITourRepository tourRepository;
    @Mock
    private IShoppingCartRepository shoppingCartRepository;
    @Mock
    private PromotionService promotionService;

    private ITourBookingService service;

    @BeforeEach
    void setUp() {
        initMocks(this);
        service = new TourBookingService(tourRepository, shoppingCartRepository, promotionService);
        when(shoppingCartRepository.saveShoppingCart(any())).then(returnsFirstArg());
        when(promotionService.calculatePromotions(any())).then(returnsFirstArg());
    }

    @Test
    void createCart() {
        // given
        when(shoppingCartRepository.saveShoppingCart(any(ShoppingCart.class))).then(returnsFirstArg());

        // when
        String shoppingCartId = service.createCart();

        // then
        ArgumentCaptor<ShoppingCart> cartCaptor = ArgumentCaptor.forClass(ShoppingCart.class);
        verify(shoppingCartRepository).saveShoppingCart(cartCaptor.capture());
        ShoppingCart shoppingCart = cartCaptor.getValue();
        assertThat(shoppingCart, is(notNullValue()));
        assertThat(shoppingCart.getShoppingCartId(), is(shoppingCartId));
    }

    @Test
    void getExistingTour() {
        // given
        Tour savedTour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        when(tourRepository.getTour(savedTour.getTourId())).thenReturn(Optional.of(savedTour));

        // when
        Tour tour = service.getTour(savedTour.getTourId());

        // then
        assertThat(tour, is(savedTour));
    }

    @Test
    void getAvailableTours() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.TEN));
        when(tourRepository.getAvailableTours()).thenReturn(Arrays.asList(tourOne, tourTwo));

        // when
        Collection<Tour> tours = service.getAvailableTours();

        // then
        assertThat(tours, hasItem(tourOne));
        assertThat(tours, hasItem(tourTwo));
    }

    @Test
    void failSingleTourBookingWhenNoShoppingCart() {
        // given
        when(shoppingCartRepository.getShoppingCart(anyString())).thenReturn(Optional.empty());

        // when + then
        assertThrows(ShoppingCartNotFoundException.class, () -> service.bookTour("TourId", "scId"));
    }

    @Test
    void bookSingleTour() {
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        Tour tour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        when(shoppingCartRepository.getShoppingCart(shoppingCart.getShoppingCartId())).thenReturn(Optional.of(shoppingCart));
        when(tourRepository.getTour(tour.getTourId())).thenReturn(Optional.of(tour));

        // when
        ShoppingCart cart = service.bookTour(tour.getTourId(), shoppingCart.getShoppingCartId());

        // then
        assertThat(cart, is(shoppingCart));
        assertThat(cart.getAllAddedTours(), hasItem(tour));
        verify(shoppingCartRepository).saveShoppingCart(shoppingCart);
    }

    @Test
    void doNotBookUnknownTour() {
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        when(shoppingCartRepository.getShoppingCart(shoppingCart.getShoppingCartId())).thenReturn(Optional.of(shoppingCart));
        when(tourRepository.getTour(anyString())).thenReturn(Optional.empty());

        // when
        ShoppingCart cart = service.bookTour("tourId", shoppingCart.getShoppingCartId());

        // then
        assertThat(cart, is(shoppingCart));
        verify(shoppingCartRepository, never()).saveShoppingCart(any(ShoppingCart.class));
    }

    @Test
    void failGetBookedToursSummaryWhenNoShoppingCart() {
        // given
        when(shoppingCartRepository.getShoppingCart(anyString())).thenReturn(Optional.empty());

        // when + then
        assertThrows(ShoppingCartNotFoundException.class, () -> service.getBookedToursSummary("scId"));
    }

    @Test
    void removeTourFromCart() {
        // given
        ShoppingCart existingCart = new ShoppingCart();
        Tour tour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        existingCart.add(tour);
        when(shoppingCartRepository.getShoppingCart(existingCart.getShoppingCartId())).thenReturn(Optional.of(existingCart));

        // when
        ShoppingCart cart = service.removeTourFromCart(tour.getTourId(), existingCart.getShoppingCartId());

        // then
        assertThat(cart, is(existingCart));
        ArgumentCaptor<ShoppingCart> cartCaptor = ArgumentCaptor.forClass(ShoppingCart.class);
        verify(shoppingCartRepository).saveShoppingCart(cartCaptor.capture());
        ShoppingCart shoppingCart = cartCaptor.getValue();
        assertThat(shoppingCart.getShoppingCartId(), is(existingCart.getShoppingCartId()));
        assertThat(shoppingCart.getAllAddedTours().size(), is(0));
    }

    @Test
    void failRemovingTourFromCartWhenNoShoppingCart() {
        // given
        when(shoppingCartRepository.getShoppingCart(anyString())).thenReturn(Optional.empty());

        // when + then
        assertThrows(ShoppingCartNotFoundException.class, () -> service.removeTourFromCart("TourId", "scId"));
    }

    @Test
    void removeAllToursFromCart() {
        // given
        ShoppingCart existingCart = new ShoppingCart();
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.TEN));
        existingCart.add(tourOne);
        existingCart.add(tourTwo);
        when(shoppingCartRepository.getShoppingCart(existingCart.getShoppingCartId())).thenReturn(Optional.of(existingCart));

        // when
        boolean result = service.removeAllToursFromCart(existingCart.getShoppingCartId());

        // then
        assertThat(result, is(true));
        ArgumentCaptor<ShoppingCart> cartCaptor = ArgumentCaptor.forClass(ShoppingCart.class);
        verify(shoppingCartRepository).saveShoppingCart(cartCaptor.capture());
        ShoppingCart shoppingCart = cartCaptor.getValue();
        assertThat(shoppingCart.getShoppingCartId(), is(existingCart.getShoppingCartId()));
        assertThat(shoppingCart.getAllAddedTours().size(), is(0));
    }

    @Test
    void failRemovingAllToursFromCartWhenNoShoppingCart() {
        // given
        when(shoppingCartRepository.getShoppingCart(anyString())).thenReturn(Optional.empty());

        // when + then
        assertThrows(ShoppingCartNotFoundException.class, () -> service.removeAllToursFromCart( "scId"));
    }
}