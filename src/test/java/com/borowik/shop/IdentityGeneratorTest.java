package com.borowik.shop;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class IdentityGeneratorTest {

    @Test
    void generateIdentityBasedOnDate() {
        // when + then
        assertThat(IdentityGenerator.generateIdentityBasedOnDate(), is(notNullValue()));
    }
}