package com.borowik.shop;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MoneyTest {

    @Test
    void failsWhenAmountIsNull() {
        // when + then
        assertThrows(NullPointerException.class, () ->
                new Money(null));
    }

    @Test
    void createMoneyWithCurrencyAUD() {
        // given
        Currency currency = Currency.getInstance("AUD");

        // when
        Money money = new Money(BigDecimal.ONE);

        // then
        assertThat(money.getAmount(), is(BigDecimal.ONE));
        assertThat(money.getCurrency(), is(currency));
    }

    @Test
    void createMoneyWithAmountZero() {
        // when
        Money money = Money.createMoneyWithAmountZero();

        // then
        assertThat(money.getAmount(), is(BigDecimal.ZERO));
        assertThat(money.getCurrency(), is(Currency.getInstance("AUD")));
    }

    @Test
    void moneyAdd() {
        // when
        Money money = new Money(BigDecimal.ONE).add(new Money(BigDecimal.ONE));

        // then
        assertThat(money.getAmount(), is(BigDecimal.valueOf(2)));
    }

    @Test
    void moneySubtract() {
        // when
        Money money = new Money(BigDecimal.TEN).subtract(new Money(BigDecimal.ONE));

        // then
        assertThat(money.getAmount(), is(BigDecimal.valueOf(9)));
    }
}