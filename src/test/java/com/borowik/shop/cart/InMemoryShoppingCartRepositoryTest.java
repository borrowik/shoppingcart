package com.borowik.shop.cart;

import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

class InMemoryShoppingCartRepositoryTest {

    private IShoppingCartRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InMemoryShoppingCartRepository();
    }

    @Test
    void addShoppingCardWithTourToRepository() {
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        Tour tour = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        shoppingCart.add(tour);

        // when
        repository.saveShoppingCart(shoppingCart);
        ShoppingCart savedTour = repository.getShoppingCart(shoppingCart.getShoppingCartId()).orElse(null);

        // then
        assertThat(savedTour, is(notNullValue()));
        assertThat(savedTour.getShoppingCartId(), is(shoppingCart.getShoppingCartId()));
        assertThat(savedTour.getAllAddedTours(), hasItem(tour));
    }
}