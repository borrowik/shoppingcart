package com.borowik.shop.cart;

import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

class ShoppingCartTest {

    private ShoppingCart cart;

    @BeforeEach
    void setUp() {
        cart = new ShoppingCart();
    }

    @Test
    void createEmptyCart() {
        // when
        cart = new ShoppingCart();

        // then
        assertThat(cart.total(), is(notNullValue()));
        assertThat(cart.total().getAmount(), is(BigDecimal.valueOf(0)));
    }

    @Test
    void returnTotalWithSingleItem() {
        // given
        Money price = new Money(BigDecimal.ONE);
        Tour tour = new Tour("HO", "Opera house tour", price);

        // when
        cart.add(tour);
        Money total = cart.total();

        // then
        assertThat(total.getAmount(), is(price.getAmount()));
    }

    @Test
    void returnTotalWithTwoItems() {
        // given
        Money priceOne = new Money(BigDecimal.ONE);
        Tour tourOne = new Tour("HO", "Opera house tour", priceOne);
        Money priceTwo = new Money(BigDecimal.TEN);
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", priceTwo);

        // when
        cart.add(tourOne);
        cart.add(tourTwo);
        Money total = cart.total();

        // then
        assertThat(total.getAmount(), is(BigDecimal.valueOf(11)));
    }

    @Test
    void returnTotalWhenRemovedTour() {
        // given
        Money priceOne = new Money(BigDecimal.ONE);
        Tour tourOne = new Tour("HO", "Opera house tour", priceOne);
        Money priceTwo = new Money(BigDecimal.TEN);
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", priceTwo);
        Money priceThree = new Money(BigDecimal.valueOf(3));
        Tour tourThree = new Tour("SK", "Sydney Sky Tower", priceThree);
        cart.add(tourOne);
        cart.add(tourTwo);
        cart.add(tourThree);
        assert BigDecimal.valueOf(14).equals(cart.total().getAmount());

        // when
        cart.remove(tourTwo);
        Money total = cart.total();

        // then
        assertThat(total.getAmount(), is(BigDecimal.valueOf(4)));
    }
}