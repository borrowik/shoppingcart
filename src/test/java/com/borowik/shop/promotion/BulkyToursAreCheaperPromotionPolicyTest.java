package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

class BulkyToursAreCheaperPromotionPolicyTest {

    private static final Tour tourHo = new Tour("HO", "Opera house tour", new Money(BigDecimal.TEN));

    private PromotionalRules promotionalRules;
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setUp() {
        promotionalRules = new BulkyToursAreCheaperPromotionPolicy(tourHo, 4, new Money(BigDecimal.ONE));
        shoppingCart = new ShoppingCart();
    }

    @Test
    void calculateNoPromotionWhenNoBonusTours() {
        // given
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.getAllAddedTours().size(), is(2));
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(20)));
    }

    @Test
    void calculatePromotionWhenFiveTours() {
        // given
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.getAllAddedTours().size(), is(5));
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(45)));
    }

}