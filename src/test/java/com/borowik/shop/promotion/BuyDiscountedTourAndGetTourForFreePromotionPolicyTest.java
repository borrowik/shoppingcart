package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

class BuyDiscountedTourAndGetTourForFreePromotionPolicyTest {

    private static final Tour tourHo = new Tour("HO", "Opera house tour", new Money(BigDecimal.TEN));
    private static final Tour tourSk = new Tour("SK", "Sydney Sky Tower", new Money(BigDecimal.ONE));

    private PromotionalRules promotionalRules;
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setUp() {
        promotionalRules = new BuyDiscountedTourAndGetTourForFreePromotionPolicy(tourHo, tourSk);
        shoppingCart = new ShoppingCart();
    }

    @Test
    void calculateNoPromotionWhenNoBonusTours() {
        // given
        shoppingCart.add(tourSk);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(1)));
        assertThat(resultCart.getAllAddedTours().size(), is(1));
    }

    @Test
    void calculatePromotionWhenTwoBonusTours() {
        // given
        shoppingCart.add(tourHo);
        shoppingCart.add(tourHo);
        shoppingCart.add(tourSk);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.getAllAddedTours().size(), is(5));
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(21)));
        assertThat(
                resultCart.getAllAddedTours().stream()
                        .filter(t -> t.getTourId().equals(tourHo.getTourId()))
                        .count()
                , is(2L));
        assertThat(
                resultCart.getAllAddedTours().stream()
                        .filter(t -> t.getTourId().equals(tourSk.getTourId()))
                        .count()
                , is(3L));
    }
}