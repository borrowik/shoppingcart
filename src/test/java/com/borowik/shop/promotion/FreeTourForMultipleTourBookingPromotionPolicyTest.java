package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

class FreeTourForMultipleTourBookingPromotionPolicyTest {

    private static final Tour tourHo = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));

    private PromotionalRules promotionalRules;
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setUp() {
        promotionalRules = new FreeTourForMultipleTourBookingPromotionPolicy(tourHo, 3);
        shoppingCart = new ShoppingCart();
    }

    @Test
    void calculateNoPromotionWhenNotEnoughTours() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.ONE));
        shoppingCart.add(tourOne);
        shoppingCart.add(tourTwo);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(2)));
    }

    @Test
    void calculateThirdTourForFree() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        shoppingCart.add(tourOne);
        shoppingCart.add(tourOne);
        shoppingCart.add(tourOne);

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(2)));
    }

    @Test
    void calculateEightTourInCart() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        IntStream.rangeClosed(1,8).forEach(i -> shoppingCart.add(tourOne));

        // when
        ShoppingCart resultCart = promotionalRules.calculatePromotion(shoppingCart);

        // then
        assertThat(resultCart.total().getAmount(), is(BigDecimal.valueOf(6)));
    }
}