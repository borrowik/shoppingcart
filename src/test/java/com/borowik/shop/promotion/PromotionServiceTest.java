package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PromotionServiceTest {

    @Mock
    PromotionalRuleFactory ruleFactory;

    private PromotionService promotionService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        promotionService = new PromotionService(ruleFactory);
    }

    @Test
    void calculatePromotionsWhenNoRulesProvided() {
        // given
        Money price = new Money(BigDecimal.ONE);
        Tour tour = new Tour("HO", "Opera house tour", price);
        ShoppingCart cart = new ShoppingCart();
        cart.add(tour);

        // when
        ShoppingCart result = promotionService.calculatePromotions(cart);

        // then
        assertThat(result, is(cart));
        assertThat(result.total(), is(price));
    }

    @Test
    void calculatePromotionsWhenRuleProvided() {
        // given
        Money price = new Money(BigDecimal.TEN);
        Tour tour = new Tour("HO", "Opera house tour", price);
        when(ruleFactory.createPromotionalRules())
                .thenReturn(new FreeTourForMultipleTourBookingPromotionPolicy(tour, 2));
        ShoppingCart cart = new ShoppingCart();
        cart.add(tour);
        cart.add(tour);

        // when
        ShoppingCart result = promotionService.calculatePromotions(cart);

        // then
        assertThat(result.getShoppingCartId(), is(cart.getShoppingCartId()));
        assertThat(result.total().getAmount(), is(price.getAmount()));
    }
}