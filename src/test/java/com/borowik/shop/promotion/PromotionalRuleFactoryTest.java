package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PromotionalRuleFactoryTest {

    @Test
    void createDefaultPromotionalRules() {
        // when
        PromotionalRules rules = new PromotionalRuleFactory().createPromotionalRules();

        // then
        assertThat(rules instanceof BulkyToursAreCheaperPromotionPolicy, is(true));
        PromotionalRules ruleTwo =  rules.getNextPromotionalRules();
        assertThat(ruleTwo instanceof BuyDiscountedTourAndGetTourForFreePromotionPolicy, is(true));
        PromotionalRules ruleThree =  ruleTwo.getNextPromotionalRules();
        assertThat(ruleThree instanceof FreeTourForMultipleTourBookingPromotionPolicy, is(true));
    }
}