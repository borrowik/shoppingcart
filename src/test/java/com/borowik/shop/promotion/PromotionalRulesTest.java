package com.borowik.shop.promotion;

import com.borowik.shop.Money;
import com.borowik.shop.cart.ShoppingCart;
import com.borowik.shop.tour.Tour;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

class PromotionalRulesTest {

    @Test
    void addChainedPromotionCalculation() {
        // given
        Tour tourOne = new Tour("HO", "Opera house tour", new Money(BigDecimal.ONE));
        Tour tourTwo = new Tour("BC", "Sydney Bridge Climb", new Money(BigDecimal.TEN));
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.add(tourOne);
        shoppingCart.add(tourOne);
        shoppingCart.add(tourOne);
        shoppingCart.add(tourOne);

        // when
        PromotionalRules promotionalRules = new FreeTourForMultipleTourBookingPromotionPolicy(tourOne, 3);
        promotionalRules.addNextPromotionRule(new BuyDiscountedTourAndGetTourForFreePromotionPolicy(tourOne, tourTwo));
        ShoppingCart result = promotionalRules.calculateNext(shoppingCart);

        // then
        assertThat(result.total().getAmount(), is(BigDecimal.valueOf(4)));
        assertThat(result.getAllAddedTours().size(), is(8));
    }
}