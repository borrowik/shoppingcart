package com.borowik.shop.tour;

import com.borowik.shop.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

class ITourRepositoryTest {

    private static final String tourIdHO = "OH";

    private ITourRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InMemoryTourRepository();
    }

    @Test
    void addTourToRepository() {
        // given
        Money price = new Money(BigDecimal.ONE);
        String tourName = "Opera house tour";
        Tour tour = new Tour(tourIdHO, tourName, price);
        assertThat(repository.getTour(tourIdHO).isPresent(), is(false));

        // when
        repository.add(tour);
        Tour savedTour = repository.getTour(tourIdHO).orElse(null);

        // then
        assertThat(savedTour, is(notNullValue()));
        assertThat(savedTour.getTourId(), is(tourIdHO));
        assertThat(savedTour.getTourName(), is(tourName));
        assertThat(savedTour.getPrice(), is(price));
    }

    @Test
    void removeTourFromRepository() {
        // given
        Money price = new Money(BigDecimal.ONE);
        String tourName = "Opera house tour";
        Tour tour = new Tour(tourIdHO, tourName, price);
        repository.add(tour);

        // when
        boolean isRemoved = repository.remove(tour);

        // then
        assertThat(isRemoved, is(true));
        assertThat(repository.getTour(tourIdHO).isPresent(), is(false));
    }

    @Test
    void returnAvailableTours() {
        // given
        Money price = new Money(BigDecimal.ONE);
        String tourName = "Opera house tour";
        Tour tourOH = new Tour(tourIdHO, tourName, price);
        Tour tourBC = new Tour("BC", tourName, price);
        repository.add(tourOH);
        repository.add(tourBC);

        // when
        Collection<Tour> tours = repository.getAvailableTours();

        // then
        assertThat(tours, is(notNullValue()));
        assertThat(tours, hasItem(tourOH));
        assertThat(tours, hasItem(tourBC));
    }

}